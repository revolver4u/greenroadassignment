package com.greenroad.candidate.assignment.evgeniymishustin;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * ContentProvider
 * Created by Evgeniy Mishustin on 02/05/2016.
 */
public class TrackingProvider extends ContentProvider {

    private static final String PROVIDER_NAME = "com.greenroad.assignment.evgeniymishustin";
    private static final String BASE_PATH = "trackings";
    static final String URL = "content://" + PROVIDER_NAME + "/" + BASE_PATH ;
    static final Uri CONTENT_URI = Uri.parse(URL);
    // used by the UriMacher
    private static final int TRACKS = 1;
    private static final int TRACK_ID = 2;

    private TrackingDbHelper database;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(PROVIDER_NAME, BASE_PATH, TRACKS);
        sURIMatcher.addURI(PROVIDER_NAME, BASE_PATH + "/#", TRACK_ID);
    }

    @Override
    public boolean onCreate() {
        database = new TrackingDbHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TrackingTable.TABLE_NAME);
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case TRACKS:
                break;
            case TRACK_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(TrackingTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, args, null, null, sortOrder);
        // make sure that listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;
        switch (uriType) {
            case TRACKS:
                id = sqlDB.insert(TrackingTable.TABLE_NAME, null, contentValues);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case TRACKS:
                rowsDeleted = sqlDB.delete(TrackingTable.TABLE_NAME, selection, selectionArgs);
                break;
            case TRACK_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TrackingTable.TABLE_NAME,
                            TrackingTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TrackingTable.TABLE_NAME,
                            TrackingTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    //wont be used in this app
    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }


}
