package com.greenroad.candidate.assignment.evgeniymishustin;

import android.database.sqlite.SQLiteDatabase;

/**
 * Represents a table in a database
 */
public class TrackingTable {

    public static final String TABLE_NAME = "points";
    public static final String COLUMN_ID = "_id";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String TIME = "time";
    public static final String SPEED = "speed";

    public static final String CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "(" + " " +
            COLUMN_ID + " " + "INTEGER PRIMARY KEY AUTOINCREMENT" + "," + " " +
            LATITUDE + " " + "REAL NOT NULL" + "," + " " + LONGITUDE + " " + "REAL NOT NULL" +
            "," + " " + TIME + " " + "INTEGER NOT NULL" + "," + " " + SPEED + " " +
            "REAL NOT NULL" + ");";

    public static void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_STATEMENT);
    }

    public static void onUpgrade(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }


}
