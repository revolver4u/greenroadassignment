package com.greenroad.candidate.assignment.evgeniymishustin;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

/**
 * Service that runs in "started" mode and writes Location changes to DataBase
 * Created by Evgeniy Mishustin on 01/05/2016.
 */
public class LocationTrackingService extends Service implements LocationListener {

    private static final int NOTIFICATION_ID = 1;
    public static boolean IS_SERVICE_RUNNING = false;

    //minimal requirements of location updates both set to zero to get the highest accuracy as asked
    private static final long MINIMAL_UPDATE_TIME = 0; //minimal time of update
    private static final float MINIMAL_DISTANCE_TO_TRACK = 0; //minimal distance
    private LocationManager locationManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) throws SecurityException {
        if (intent.getAction().equals(MainActivity.START_FOREGROUND_ACTION)) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMAL_UPDATE_TIME, MINIMAL_DISTANCE_TO_TRACK, this);
            startForeground(NOTIFICATION_ID, buildNotification());
        }else if(intent.getAction().equals(MainActivity.STOP_FOREGROUND_ACTION)) {
            locationManager.removeUpdates(this);
            stopForeground(true);
            stopSelf();
        }
            return START_STICKY;
        }

        @Override
        public void onDestroy ()throws SecurityException {
            super.onDestroy();
        }

        @Override
        public IBinder onBind (Intent intent){
            return null;
        }


        @Override
        public void onLocationChanged (Location location){
            getContentResolver().insert(TrackingProvider.CONTENT_URI, createValuesFromLocation(location));
        }

        @Override
        public void onStatusChanged (String s,int i, Bundle bundle){

        }

        @Override
        public void onProviderEnabled (String s){

        }

        @Override
        public void onProviderDisabled (String s){

        }

    /*
     * Adapts a location object to ContentValues object
     */

    private ContentValues createValuesFromLocation(Location location) {
        ContentValues cv = new ContentValues();
        cv.put(TrackingTable.LATITUDE, location.getLatitude());
        cv.put(TrackingTable.LONGITUDE, location.getLongitude());
        cv.put(TrackingTable.TIME, location.getTime());
        cv.put(TrackingTable.SPEED, location.getSpeed());
        return cv;
    }

    private Notification buildNotification(){
        return new NotificationCompat.Builder(this)
                .setContentTitle("GPS Tracking Enabled")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("Click here to open application")
                .setContentIntent(buildIntent()).build();

    }

    /*
     * build intent that will be triggered on notification click
     */
    private PendingIntent buildIntent(){
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
    }
}
