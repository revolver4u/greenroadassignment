package com.greenroad.candidate.assignment.evgeniymishustin;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, OnMapReadyCallback {

    private static final int LOCATION_PERMISSION = 1;
    public static String START_FOREGROUND_ACTION = "com.greenroad.candidate.assignment.evgeniymishustin.startforeground";
    public static String STOP_FOREGROUND_ACTION = "com.greenroad.candidate.assignment.evgeniymishustin.stopforeground";

    private GoogleMap map;
    private TextView time;
    private SimpleDateFormat sf = new SimpleDateFormat("HH:mm:ss");

    private TextView speed;
    private TextView latitude;
    private TextView longitude;

    private ToggleButton start;

    private boolean FROM_BACKGROUND;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FROM_BACKGROUND = true;

        time = (TextView) findViewById(R.id.time);
        speed = (TextView)findViewById(R.id.speed);
        latitude = (TextView)findViewById(R.id.latitude);
        longitude = (TextView) findViewById(R.id.longitude);

        start = (ToggleButton) findViewById(R.id.button_start_tracking);
        //update the button state in case activity restarted but service running
        if (!LocationTrackingService.IS_SERVICE_RUNNING) {
            start.setChecked(false);
        } else {
            start.setChecked(true);
        }
        start.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    Intent startIntent = new Intent(MainActivity.this, LocationTrackingService.class);
                    startIntent.setAction(START_FOREGROUND_ACTION);
                    startService(startIntent);
                    LocationTrackingService.IS_SERVICE_RUNNING = true;
                } else {
                    getContentResolver().delete(TrackingProvider.CONTENT_URI, null, null);
                    Intent stopIntent = new Intent(MainActivity.this, LocationTrackingService.class);
                    stopIntent.setAction(STOP_FOREGROUND_ACTION);
                    startService(stopIntent);
                    stopService(new Intent(getApplicationContext(), LocationTrackingService.class));
                    LocationTrackingService.IS_SERVICE_RUNNING = false;
                }

            }
        });


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getSupportLoaderManager().initLoader(0, null, this);


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /*
     * OnMapReady initialize the map, check for permissions and try to animate to current Location
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION);
            return;
        }else{
            tryToAnimateToCurrentLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case (LOCATION_PERMISSION):
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start.setEnabled(true);
                    tryToAnimateToCurrentLocation();
                } else {
                    //disable tracking option on permission denied
                    start.setEnabled(false);
                }
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                TrackingTable.COLUMN_ID,
                TrackingTable.LATITUDE,
                TrackingTable.LONGITUDE,
                TrackingTable.SPEED,
                TrackingTable.TIME
        };
        CursorLoader cursorLoader = new CursorLoader(this, TrackingProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        System.out.println(data.getCount() + " rows in DB");
        if (data.getCount() > 0) {
            //redraw all the map only if we are coming from background
            if(FROM_BACKGROUND){
                PolylineOptions polylineOptions = new PolylineOptions().width(3).color(Color.BLUE);

                //draw a line
                if (map != null) {
                    map.clear();
                }
                while (data.moveToNext()) {
                    polylineOptions.add(new LatLng(data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LATITUDE)), data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LONGITUDE))));
                }
                if (map != null) {
                    map.addPolyline(polylineOptions);
                }
                FROM_BACKGROUND = false;
                //otherwise add only last segment
            }else{
                PolylineOptions polylineOptions = new PolylineOptions().width(3).color(Color.BLUE);
                data.moveToLast();
                data.moveToPrevious();
                polylineOptions.add(new LatLng(data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LATITUDE)), data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LONGITUDE))));
                data.moveToLast();
                polylineOptions.add(new LatLng(data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LATITUDE)), data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LONGITUDE))));
                if(map!=null){
                    map.addPolyline(polylineOptions);
                }
            }


            data.moveToLast();
            Date d = new Date(data.getLong(data.getColumnIndexOrThrow(TrackingTable.TIME)));
            time.setText("Time: " + sf.format(d));
            String speedValue = String.valueOf(data.getFloat(data.getColumnIndexOrThrow(TrackingTable.SPEED)));
            speed.setText("Speed: " + speedValue);
            String latValue = String.valueOf(data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LATITUDE)));
            latitude.setText("Latitude " + latValue);
            String longValue = String.valueOf(data.getDouble(data.getColumnIndexOrThrow(TrackingTable.LONGITUDE)));
            longitude.setText("Longitude " + longValue);

        } else {
            time.setText("No tracking yet");
            speed.setText(null);
            if (map != null) {
                map.clear();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /*
     * On start we want to animate to current position
     */
    private void tryToAnimateToCurrentLocation() throws SecurityException{
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = service.getBestProvider(criteria, false);
        Location location = service.getLastKnownLocation(provider);
        if (location != null) {
            double mCurrentLocationLongitude = location.getLongitude();
            double mCurrentLocationLatitude = location.getLatitude();
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(mCurrentLocationLatitude, mCurrentLocationLongitude)).zoom(14).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            map.setMyLocationEnabled(true);
        }
    }
}
